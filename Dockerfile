FROM mcr.microsoft.com/dotnet/sdk:6.0

COPY DotnetTemplate.Web ./DotnetTemplate.Web

RUN cd DotnetTemplate.Web; dotnet build

RUN apt-get update && apt-get install -y build-essential
RUN curl -fsSL https://deb.nodesource.com/setup_18.x | bash -
RUN apt-get install -y nodejs

RUN cd DotnetTemplate.Web && npm ci && npm run build

WORKDIR /DotnetTemplate.Web

ENTRYPOINT ["dotnet", "run"]